PROJECT=main
LANG=es

.PHONY: all clean check

all: $(PROJECT).pdf

# MAIN LATEXMK RULE
$(PROJECT).pdf: $(PROJECT).tex macros.tex
	latexmk -f -bibtex -use-make -pdf -pdflatex="pdflatex -synctex=1" $(PROJECT).tex

check: $(PROJECT).tex
	aspell --lang=$(LANG) -t -c $(PROJECT).tex

clean:
	latexmk -C
